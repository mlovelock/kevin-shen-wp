<?php
// Register widgetized areas
if ( ! function_exists( 'ah_widgets_init' ) ) {
	function ah_widgets_init() {
	    if ( ! function_exists( 'register_sidebars' ) )
	        return;
	
		// Widgetized sidebars
	    register_sidebar( array( 'name' => __( 'Homepage LH', 'woothemes' ), 'id' => 'home-lh', 'description' => __( 'The lower left hand widget area for the home page.', 'woothemes' ), 'before_widget' => '<div id="%1$s" class="widget %2$s">', 'after_widget' => '</div>', 'before_title' => '<h3>', 'after_title' => '</h3>' ) );
	    register_sidebar( array( 'name' => __( 'Homepage RH', 'woothemes' ), 'id' => 'home-rh', 'description' => __( 'The lower right hand widget area for the home page', 'woothemes' ), 'before_widget' => '<div id="%1$s" class="widget %2$s">', 'after_widget' => '</div>', 'before_title' => '<h3>', 'after_title' => '</h3>' ) );
	
	} // End ah_widgets_init()
}

add_action( 'init', 'ah_widgets_init' );  


function ah_googlefonts(){
?>
<link href='http://fonts.googleapis.com/css?family=Belleza' rel='stylesheet' type='text/css'>
<?php
}
add_action( 'woo_head', 'ah_googlefonts' );

add_filter( 'woo_footer_right', 'ah_footertext', 20 );

if ( function_exists( 'wp_nav_menu') ) {
	add_theme_support( 'nav-menus' );
	register_nav_menus( array( 'footer-menu' => __( 'Footer Menu', 'woothemes' ) ) );
}

function ah_footertext($html){

	$html = '';
	$html .= ' <div style="text-align: right;">';
	$html .= wp_nav_menu( array( 'depth' => 6, 'sort_column' => 'menu_order', 'container' => 'ul', 'container_class' => 'nav', 'menu_id' => 'footer-nav', 'menu_class' => 'nav', 'theme_location' => 'footer-menu', 'after' => ' :: ', 'echo' => '0' ) );
	//$html = '<p><a href="'. get_bloginfo('url') .'/sitemap/">Site Map</a> :: ';
	$html .= '<p style="float: right; display: inline-block;">Built &amp; Hosted by <a href="http://www.artshosting.co" title="Arts Hosting" rel="nofollow">Arts Hosting</a></p>';
	$html .= '</div>';
	
	return $html;

}

/*-----------------------------------------------------------------------------------*/
/* Woo Slider Setup */
/*-----------------------------------------------------------------------------------*/
if ( ! function_exists( 'woo_slider' ) ) {
	function woo_slider( $load_slider_js = false ) {
		global $woo_options;

		$load_slider_js = false;

		if ( ( is_page_template( 'template-biz.php' ) && isset( $woo_options['woo_slider_biz'] ) && $woo_options['woo_slider_biz'] == 'true' ) ||
			 ( is_page_template( 'template-home.php' ) && isset( $woo_options['woo_slider_biz'] ) && $woo_options['woo_slider_biz'] == 'true' ) ||
			 ( is_page_template( 'template-magazine.php' ) && isset( $woo_options['woo_slider_magazine'] ) && $woo_options['woo_slider_magazine'] == 'true' ) ||
			   is_page_template( 'template-widgets.php' ) ||
			   is_active_sidebar( 'homepage' ) ) { $load_slider_js = true; }

		// Allow child themes/plugins to load the slider JavaScript when they need it.
		$load_slider_js = (bool)apply_filters( 'woo_load_slider_js', $load_slider_js );


		if ( $load_slider_js != false ) {

		// Default slider settings.
		$defaults = array(
							'autoStart' => 0,
							'autoHeight' => 'false',
							'hoverPause' => 'false',
							'containerClick' => 'false',
							'slideSpeed' => 600,
							'canAutoStart' => 'false',
							'next' => 'next',
							'prev' => 'previous',
							'container' => 'slides',
							'generatePagination' => 'false',
							'crossfade' => 'true',
							'fadeSpeed' => 600,
							'effect' => 'slide'
						 );

		// Dynamic settings from the "Theme Options" screen.
		$args = array();

		if ( isset( $woo_options['woo_slider_pagination'] ) && $woo_options['woo_slider_pagination'] == 'true' ) { $args['generatePagination'] = 'true'; }
		if ( isset( $woo_options['woo_slider_effect'] ) && $woo_options['woo_slider_effect'] != '' ) { $args['effect'] = $woo_options['woo_slider_effect']; }
		if ( isset( $woo_options['woo_slider_autoheight'] ) && $woo_options['woo_slider_autoheight'] == 'true' ) { $args['autoHeight'] = 'true'; }
		if ( isset( $woo_options['woo_slider_hover'] ) && $woo_options['woo_slider_hover'] == 'true' ) { $args['hoverPause'] = 'true'; }
		if ( isset( $woo_options['woo_slider_containerclick'] ) && $woo_options['woo_slider_containerclick'] == 'true' ) { $args['containerClick'] = 'true'; }
		if ( isset( $woo_options['woo_slider_speed'] ) && $woo_options['woo_slider_speed'] != '' ) { $args['slideSpeed'] = $woo_options['woo_slider_speed'] * 1000; }
		if ( isset( $woo_options['woo_slider_speed'] ) && $woo_options['woo_slider_speed'] != '' ) { $args['fadeSpeed'] = $woo_options['woo_slider_speed'] * 1000; }
		if ( isset( $woo_options['woo_slider_auto'] ) && $woo_options['woo_slider_auto'] == 'true' ) {
			$args['canAutoStart'] = 'true';
			$args['autoStart'] = $woo_options['woo_slider_interval'] * 1000;
		}

		// Merge the arguments with defaults.
		$args = wp_parse_args( $args, $defaults );

		// Allow child themes/plugins to filter these arguments.
		$args = apply_filters( 'woo_slider_args', $args );

	?>
	<!-- Woo Slider Setup -->
	<script type="text/javascript">
	jQuery(window).load(function() {
		var args = {};
		args.useCSS = false;
		<?php if ( $args['effect'] == 'fade' ) { ?>args.animation = 'fade';
		<?php } else { ?>args.animation = 'slide';<?php } ?>
		<?php echo "\n"; ?>
		<?php if ( $args['canAutoStart'] == 'true' ) { ?>args.slideshow = true;
		<?php } else { ?>args.slideshow = false;<?php } ?>
		<?php echo "\n"; ?>
		<?php if ( intval( $args['autoStart'] ) > 0 ) { ?>args.slideshowSpeed = <?php echo intval( $args['autoStart'] ) ?>;<?php } ?>
		<?php echo "\n"; ?>
		<?php if ( intval( $args['slideSpeed'] ) >= 0 ) { ?>args.animationSpeed = <?php echo intval( $args['slideSpeed'] ) ?>;<?php } ?>
		<?php echo "\n"; ?>
		<?php if ( $args['generatePagination'] == 'true' ) { ?>args.controlNav = true;
		<?php } else { ?>args.controlNav = false;<?php } ?>
		<?php echo "\n"; ?>
		<?php if ( $args['hoverPause'] == 'true' ) { ?>args.pauseOnHover = true;
		<?php } else { ?>args.pauseOnHover = false;<?php } ?>
		<?php echo "\n"; ?>
		<?php if ( $args['autoHeight'] == 'true' ) { ?>args.smoothHeight = true;<?php } ?>
		args.manualControls = '.pagination-wrap .flex-control-nav > li';
		args.start = function ( slider ) {
			slider.next( '.slider-pagination' ).fadeIn();
		}
		args.prevText = '<span class="icon-angle-left"></span>';
		args.nextText = '<span class="icon-angle-right"></span>';

		jQuery( '.woo-slideshow' ).each( function ( i ) {
			jQuery( this ).flexslider( args );
		});
	});
	</script>
	<!-- /Woo Slider Setup -->
	<?php
		}
	} // End woo_slider()
}

/*-----------------------------------------------------------------------------------*/
/* Woo Slider Business */
/*-----------------------------------------------------------------------------------*/

if ( ! function_exists( 'woo_slider_biz' ) ) {
	function woo_slider_biz( $args = null ) {

		global $woo_options, $post;

		// Exit if this isn't the first page in the loop
		if ( is_paged() ) return;

		$options = woo_get_dynamic_values( array( 'slider_biz_slide_group' => '0' ) );

		// Default slider settings.
		$defaults = array(
							'id' => 'loopedSlider',
							'pagination' => false,
							'width' => '960',
							'height' => '350',
							'order' => 'ASC',
							'posts_per_page' => '5',
							'slide_page' => $options['slider_biz_slide_group'],
							'use_slide_page' => false
						 );

		if ( '0' != $defaults['slide_page'] ) $defaults['use_slide_page'] = true;

		// Setup the "Slide Group", if one is set.
		if ( isset( $post->ID ) ) {
			$slide_page = '0';
			$stored_slide_page = get_post_meta( $post->ID, '_slide-page', true );

			if ( $stored_slide_page != '' && '0' != $stored_slide_page ) {
				$slide_page = $stored_slide_page;
				$defaults['use_slide_page'] = true; // Instruct the slider to apply the necessary conditional.
				$defaults['slide_page'] = $slide_page;
			}
		}

		// Setup height of slider.
		$height = $woo_options['woo_slider_biz_height'];
		if ( $height != '' ) { $defaults['height'] = $height; }

		// Setup width of slider and images.
		if ( isset( $woo_options['woo_slider_biz_full'] ) && 'true' == $woo_options['woo_slider_biz_full'] ) {
			$width = '1600';
		} else {
			$layout = $woo_options['woo_layout'];
			if ( !$layout )
				$layout = get_option( 'woo_layout' );
			$layout_width = get_option('woo_layout_width');
	
			$width = intval( $layout_width );
		}

		// Setup the number of posts to show.
		$posts_per_page = $woo_options['woo_slider_biz_number'];
		if ( $posts_per_page != '' ) { $defaults['posts_per_page'] = $posts_per_page; }

		// Setup the order of posts.
		$post_order = $woo_options['woo_slider_biz_order'];
		if ( $post_order != '' ) { $defaults['order'] = $post_order; }

		if ( ( 0 < $width ) && !isset( $args['width'] ) ) { $defaults['width'] = $width; }

		// Merge the arguments with defaults.
		$args = wp_parse_args( $args, $defaults );

		if ( ( ( isset( $args['width'] ) ) && ( ( $args['width'] <= 0 ) || ( $args['width'] == '' )  ) ) || ( ! isset( $args['width'] ) ) ) {	$args['width'] = '100'; }
		if ( ( isset( $args['height'] ) ) && ( $args['height'] <= 0 )  ) { $args['height'] = '100'; }

		// Allow child themes/plugins to filter these arguments.
		$args = apply_filters( 'woo_biz_slider_args', $args );

		// Disable auto image functionality
		$auto_img = false;
		if ( get_option( 'woo_auto_img' ) == 'true' ) {
			update_option( 'woo_auto_img', 'false' );
			$auto_img = true;
		}

		// Disable placeholder image functionality
		$placeholder_img = get_option( 'framework_woo_default_image' );
		if ( $placeholder_img ) {
			update_option( 'framework_woo_default_image', '' );
		}

		// Setup the slider CSS class.
		$slider_css = '';
		if ( isset( $woo_options['woo_slider_pagination'] ) && $woo_options['woo_slider_pagination'] == 'true' ) {
			$slider_css = 'business-slider has-pagination woo-slideshow';
		} else {
			$slider_css = 'business-slider woo-slideshow';
		}

		// Setup the slider height.
		if ( isset( $woo_options['woo_slider_autoheight'] ) && $woo_options['woo_slider_autoheight'] != 'true' ) {
//			$slider_height = $args['height'];
			$slider_height = '420';
	    } else {
			$slider_height = 'height:auto';
		}

		// Slide Styles
		$slide_styles = 'width: ' . $args['width'] . 'px;';

		$query_args = array(
						'posts_per_page' => $posts_per_page,
						'order' => $post_order,
						'use_slide_page' => $args['use_slide_page'],
						'slide_page_terms' => $args['slide_page']
					);

		// Retrieve the slides, based on the query arguments.
		$slides = woo_slider_get_slides( $query_args );

		if ( false == $slides ) {
			echo do_shortcode( '[box type="alert"]' . __( 'Please add some slider posts via Slides > Add New', 'woothemes' ) . '[/box]'); 
			return;
		}

		if ( ( count( $slides ) < 1 ) ) {
			echo do_shortcode( '[box type="alert"]' . __( 'Please note that this slider requires 2 or more slides in order to function. Please add another slide.', 'woothemes' ) . '[/box]'); 
			return;
		}

		$view_args = array(
					'id' => $args['id'],
//					'width' => $args['width'],
//					'height' => $slider_height,
'width' => '823',
'height' => '420',
					'container_css' => $slider_css,
					'slide_styles' => $slide_styles
				);

		// Allow child themes/plugins to filter these arguments.
		$view_args = apply_filters( 'woo_slider_biz_view_args', $view_args );		

		// Display slider
		woo_slider_biz_view( $view_args, $slides );

		// Enable auto img functionality
		if ( $auto_img )
			update_option( 'woo_auto_img', 'true' );

		// Enable placeholder functionality
		if ( '' != $placeholder_img )
			update_option( 'framework_woo_default_image', $placeholder_img );

	} // End woo_slider_biz()
}


/*-----------------------------------------------------------------------------------*/
/* Woo Business Slider View */
/*-----------------------------------------------------------------------------------*/
if ( ! function_exists( 'woo_slider_biz_view' ) ) {
	function woo_slider_biz_view( $args = null, $slides = null ) {

		global $woo_options, $post;

		// Default slider settings.
		$defaults = array(
							'id' => 'loopedSlider',
							'width' => '960',
							'height' => '350',
							'container_css' => '',
							'slide_styles' => ''
						);

		// Merge the arguments with defaults.
		$args = wp_parse_args( $args, $defaults );		

		// Init slide count
		$count = 0;

	?>

	<?php do_action('woo_biz_slider_before'); ?>

	<div id="<?php echo esc_attr( $args['id'] ); ?>"<?php if ( '' != $args['container_css'] ): ?> class="<?php echo esc_attr( $args['container_css'] ); ?>"<?php endif; ?><?php if ( '' != $args['height'] ): ?> style="height: <?php echo esc_attr( $args['height'] ); ?>px;"<?php endif; ?>>

		<ul class="slides"<?php if ( '' != $args['height'] ): ?> style="height: <?php echo $args['height']; ?>px;"<?php endif; ?>>

			<?php foreach ( $slides as $k => $post ) { setup_postdata( $post ); $count++; ?>

			<?php 
				// Slide Styles
				if ( $count >= 2 ) { $args['slide_styles'] .= ' display:none;'; } else { $args['slide_styles'] = ''; }
			?>

			<li id="slide-<?php echo esc_attr( $post->ID ); ?>" class="slide slide-number-<?php echo esc_attr( $count ); ?>" <?php if ( '' != $args['slide_styles'] ): ?>style="<?php echo esc_attr( $args['slide_styles'] ); ?>"<?php endif; ?>>

				<?php
					$type = woo_image('return=true');
					if ( $type ):
						$url = get_post_meta( $post->ID, 'url', true );
				?>

					<?php if ( '' != $url ): ?><a href="<?php echo esc_url( $url ); ?>" title="<?php the_title_attribute(); ?>"><?php endif; ?>
					<?php woo_image( 'width=623&height=' . $args['height'] . '&link=img&noheight=true' ); ?>
					<?php if ( '' != $url ): ?></a><?php endif; ?>

						<?php if ( 'true' == $woo_options['woo_slider_biz_title'] ): ?>
					<div class="content">

						<?php if ( 'true' == $woo_options['woo_slider_biz_title'] ): ?>
						<div class="title">
							<h2 class="title">
								<?php if ( '' != $url ): ?><a href="<?php echo esc_url( $url ); ?>" title="<?php the_title_attribute(); ?>"><?php endif; ?>
								<?php the_title(); ?>
								<?php if ( '' != $url ): ?></a><?php endif; ?>
							</h2>
						</div>
						<?php endif; ?>

						<div class="excerpt">
							<?php 
		 						$content = get_the_excerpt();
								$content = do_shortcode( $content );
								echo wpautop( $content );
							?>
						</div><!-- /.excerpt -->

					</div><!-- /.content -->
						<?php endif; ?>

				<?php else: ?>

					<section class="entry col-full">
						<?php the_content(); ?>
					</section>

				<?php endif; ?>

			</li><!-- /.slide-number-<?php echo esc_attr( $count ); ?> -->

			<?php } // End foreach ?>

			<?php wp_reset_postdata();  ?>

		</ul><!-- /.slides -->

	</div><!-- /#<?php echo $args['id']; ?> -->

	<?php if ( isset( $woo_options['woo_slider_pagination'] ) && $woo_options['woo_slider_pagination'] == 'true' ) : ?>
		<div class="pagination-wrap slider-pagination">
			<ol class="flex-control-nav flex-control-paging">
				<?php for ( $i = 0; $i < $count; $i++ ): ?>
					<li><a><?php echo ( $i + 1 ) ?></a></li>
				<?php endfor; ?>
			</ol>
		</div>
	<?php endif; ?>

	<?php do_action('woo_biz_slider_after'); ?>

<?php
	} // End woo_slider_biz_view()
}


?>